import Rainy from '../components/Rainy';
import { SwitchNavigator } from '@react-navigation/native';

export const route = () => SwitchNavigator(
    {
      Rainy: Rainy
    },
    {
      initialRouteName: 'Rainy',
    }
  );