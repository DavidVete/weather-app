import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Image,ImageBackground, ScrollView } from 'react-native';

export default function App() {
  const [weatherData, setWeatherData] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [background, setBackground] = useState("#FFFFFF");
  const [location, setLocation] = useState();

  if (location == null) {
    navigator.geolocation.getCurrentPosition(
      position => {
        setLocation(position);
      },
      error => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }
  
  if (weatherData == null && location != null) {
    fetch('https://api.openweathermap.org/data/2.5/onecall?lat='.concat(location.coords.latitude.toString()) + '&lon='.concat(location.coords.longitude.toString()) + '&exclude=hourly,minutely,alerts&units=metric&appid=1a3c5b1b51392b32c60bd6e8d4b45e3c',
    {
       method: 'GET'
    })
    .then((response) => response.json())
    .then((responseJson) => {
       setIsLoading(false);
       setWeatherData(responseJson);
       if(responseJson != null) {
        if (responseJson.current.weather.main == "Rain") {
          setBackground("#57575D")
        } else if (responseJson.current.weather.main == "Drizzle") {
          setBackground("#57575D")
        } else if (responseJson.current.weather.main == "Thunderstorm") {
          setBackground("#57575D")
        } else if (responseJson.current.weather.main == "Clear") {
          setBackground("#47AB2F")
        } else {
          setBackground("#54717A")
        }
      }
    })
    .catch((error) => {
       setWeatherData("error")
       setIsLoading(false);
    });
  }
  
  const Rainy = () => {
    return (
      <View style={styles.container(background)}>
        <ImageBackground source={require('./assets/forest_rainy.png')} style={styles.stretch}>
          <Text style={styles.text}>{weatherData.current.temp.toFixed()}&deg;</Text>
        </ImageBackground>
        <Text style={ {marginTop: 110,
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center"}}>Rainy</Text>
    </View>
    )
  }

  const Sunny = () => {
    return (
      <View style={styles.container(background)}>
        <ImageBackground source={require('./assets/forest_sunny.png')} style={styles.stretch}>
          <Text style={styles.text}>{weatherData.current.temp.toFixed()}&deg;</Text>
        </ImageBackground>
        <Text style={ {marginTop: 110,
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center"}}>Sunny</Text>
    </View>
    )
  }

  const Cloudy = () => {
    return (
      <View style={styles.container(background)}>
        <ImageBackground source={require('./assets/forest_cloudy.png')} style={styles.stretch}>
          <Text style={styles.text}>{weatherData.current.temp.toFixed()}&deg;</Text>
        </ImageBackground>
        <Text style={ {marginTop: 110,
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center"}}>Cloudy</Text>
    </View>
    )
  }

  const Weather = () => {
    if (weatherData.current.weather.main == "Rain") {
      return <Rainy/>
    } else if (weatherData.current.weather.main == "Drizzle") {
      return <Rainy/>
    } else if (weatherData.current.weather.main == "Thunderstorm") {
      return <Rainy/>
    } else if (weatherData.current.weather.main == "Clear") {
      return <Sunny/>
    } else {
      return <Cloudy/>
    }
  }

  const Current = () => {
    return (
      <View style={{flexDirection: 'row',justifyContent: "space-between",marginTop: 310, marginEnd: 10, marginStart: 10}}>
        <View>
          <Text style={{color: "white"}}>{weatherData.daily[0].temp.min.toFixed()}&deg;</Text>
          <Text style={{color: "white"}}>min</Text>
        </View>
        <View>
          <Text style={{color: "white"}}>{weatherData.current.temp.toFixed()}&deg;</Text>
          <Text style={{color: "white"}}>Current</Text>
        </View>
        <View>
          <Text style={{color: "white"}}>{weatherData.daily[0].temp.max.toFixed()}&deg;</Text>
          <Text style={{color: "white"}}>max</Text>
        </View>
      </View>
    )
  }

  const DailyForecast = (props, key) => {
    if (props.forecast.weather[0].main == "Rain") {
      let logo = './assets/rain.png'
      return (
        <View style={{flexDirection: "row" , justifyContent:"space-between", marginTop: 10, marginEnd: 10, marginStart: 10}} key = {key}>
          <Text style={{color: "white", alignContent: "center", justifyContent: "center"}}>{getDayOfWeek((new Date(props.forecast.dt * 1000)).getDay())}</Text>
          <View>
           <Image
           source = {require(logo)}
           />
          </View> 
           <View>
            <Text style={{color: "white"}}>{props.forecast.temp.day.toFixed()}&deg;</Text>
          </View>
        </View>
      )
    } else if (props.forecast.weather[0].main == "Drizzle") {
    let logo = './assets/rain.png'
    return (
      <View style={{flexDirection: "row" , justifyContent:"space-between", marginTop: 10, marginEnd: 10, marginStart: 10}} key = {key}>
        <Text style={{color: "white", alignContent: "center", justifyContent: "center"}}>{getDayOfWeek((new Date(props.forecast.dt * 1000)).getDay())}</Text>
        <View>

         <Image
         source = {require(logo)}
         />
        </View> 
         <View>
          <Text style={{color: "white"}}>{props.forecast.temp.day.toFixed()}&deg;</Text>
        </View>
      </View>
    )
    } else if (props.forecast.weather[0].main == "Thunderstorm") {
    let logo = './assets/rain.png'
    return (
      <View style={{flexDirection: "row" , justifyContent:"space-between", marginTop: 10, marginEnd: 10, marginStart: 10}} key = {key}>
        <Text style={{color: "white", alignContent: "center", justifyContent: "center"}}>{getDayOfWeek((new Date(props.forecast.dt * 1000)).getDay())}</Text>
        <View>
         <Image
         source = {require(logo)}
         />
        </View> 
         <View>
          <Text style={{color: "white"}}>{props.forecast.temp.day.toFixed()}&deg;</Text>
        </View>
      </View>
    )
    } else if (props.forecast.weather[0].main == "Clear") {
      let logo = './assets/clear.png'
      return (
        <View style={{flexDirection: "row" , justifyContent:"space-between", marginTop: 10, marginEnd: 10, marginStart: 10}} key = {key}>
          <Text style={{color: "white", alignContent: "center", justifyContent: "center"}}>{getDayOfWeek((new Date(props.forecast.dt * 1000)).getDay())}</Text>
          <View>
           <Image
           source = {require(logo)}
           />
          </View> 
           <View>
            <Text style={{color: "white"}}>{props.forecast.temp.day.toFixed()}&deg;</Text>
          </View>
        </View>
      )
    } else {
    let logo = './assets/partlysunny.png'
    return (
      <View style={{flexDirection: "row" , justifyContent:"space-between", marginTop: 10, marginEnd: 10, marginStart: 10}} key = {key}>
        <Text style={{color: "white", alignContent: "center", justifyContent: "center"}}>{getDayOfWeek((new Date(props.forecast.dt * 1000)).getDay())}</Text>
        <View>
         <Image
         source = {require(logo)}
         />
        </View> 
         <View>
          <Text style={{color: "white"}}>{props.forecast.temp.day.toFixed()}&deg;</Text>
        </View>
      </View>
    )
    }
  }

  const render = (props) => {}

  const Forecast = () => {
    const views = []
    for(let day = 1; day <= 5; day++){
      views.push(<DailyForecast forecast = {weatherData.daily[day]} key = {day}/>)
    }  
    return views
  }
  
  return (
    <View style={styles.container(background)}>
      {isLoading ?
      <View>
        <Text style={styles.text_normal}>loading...</Text>
        <StatusBar style="auto" />
      </View > 
      : 
      weatherData == null ?
      <View>
        <Text style={styles.text_normal}>Sorry, cannot get weather information at the moment</Text>
        <StatusBar style="auto" />
      </View > 
      :
      <View>
      <Weather/>
      <Current/>
      <View style = {styles.lineStyle} />
      <ScrollView>
        <Forecast/>
      </ScrollView>
    </View> }
    </View>
  );
}

const styles  = StyleSheet.create({
  container: backgroundColor => ({
    flex: 1,
    flexDirection: "column",
    backgroundColor: backgroundColor,
    width: '100%',
    height: 700
  }),
  header : {
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  stretch: {
    width: '100%',
    height: 300,
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    marginTop: 150,
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center"
  },
  text_normal: {
    marginTop: 270,
    color: "black",
    fontSize: 20,
    textAlign: "center"
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'white',
    marginTop: 10
}
});

const getDayOfWeek = (num) => {
  var weekday = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  return weekday[num];
}
